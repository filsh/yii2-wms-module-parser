<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\parser\models\Parser\Attribute as ParserAttribute;
use wms\parser\models\Parser\Attribute\Rule as ParserAttributeRule;
use wms\parser\models\Parser\Attribute\Rule\Type as ParserAttributeRuleType;

class m151104_112154_create_parser_attribute_rules_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(ParserAttributeRule::tableName(), [
            'parser_attribute_rule_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'parser_attribute_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'parser_attribute_rule_type_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'parser_attribute_rule_settings' => Schema::TYPE_TEXT . ' NOT NULL',
            'FOREIGN KEY (`parser_attribute_rule_type_id`) REFERENCES ' . ParserAttributeRuleType::tableName() . ' (`parser_attribute_rule_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
            'FOREIGN KEY (`parser_attribute_id`) REFERENCES ' . ParserAttribute::tableName() . ' (`parser_attribute_id`) ON DELETE NO ACTION ON UPDATE NO ACTION'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(ParserAttributeRule::tableName());
    }
}
