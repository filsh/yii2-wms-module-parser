<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\parser\models\Parser\Type as ParserType;

class m151104_112109_create_parser_types_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(ParserType::tableName(), [
            'parser_type_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'parser_type_name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'parser_type_title' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(ParserType::tableName());
    }
}
