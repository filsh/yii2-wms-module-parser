<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\parser\models\Parser\Attribute\Rule\Type as ParserAttributeRuleType;

class m151104_112148_create_parser_attribute_rule_types_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(ParserAttributeRuleType::tableName(), [
            'parser_attribute_rule_type_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'parser_attribute_rule_type_name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'parser_attribute_rule_type_title' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(ParserAttributeRuleType::tableName());
    }
}
