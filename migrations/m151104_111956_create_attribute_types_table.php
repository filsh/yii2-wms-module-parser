<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\parser\models\Attribute\Type;

class m151104_111956_create_attribute_types_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(Type::tableName(), [
            'attribute_type_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'attribute_type_name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'attribute_type_title' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(Type::tableName());
    }
}
