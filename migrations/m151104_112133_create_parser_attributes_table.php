<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\parser\models\Attribute;
use wms\parser\models\Parser;
use wms\parser\models\Parser\Attribute as ParserAttribute;

class m151104_112133_create_parser_attributes_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(ParserAttribute::tableName(), [
            'parser_attribute_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'parser_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'attribute_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'UNIQUE INDEX (`parser_id`, `attribute_id`)',
            'FOREIGN KEY (`attribute_id`) REFERENCES ' . Attribute::tableName() . ' (`attribute_id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
            'FOREIGN KEY (`parser_id`) REFERENCES ' . Parser::tableName() . ' (`parser_id`) ON DELETE NO ACTION ON UPDATE NO ACTION'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(ParserAttribute::tableName());
    }
}
