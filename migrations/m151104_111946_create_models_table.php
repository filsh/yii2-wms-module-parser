<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\parser\models\Model;
use wms\parser\models\Owner;
use wms\parser\models\Category;

class m151104_111946_create_models_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(Model::tableName(), [
            'model_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'owner_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'category_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'model_name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'FOREIGN KEY (`category_id`) REFERENCES ' . Category::tableName() . ' (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
            'FOREIGN KEY (`owner_id`) REFERENCES ' . Owner::tableName() . ' (`owner_id`) ON DELETE NO ACTION ON UPDATE NO ACTION'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(Model::tableName());
    }
}
