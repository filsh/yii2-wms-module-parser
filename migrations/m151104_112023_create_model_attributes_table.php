<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\parser\models\Model;
use wms\parser\models\Attribute;
use wms\parser\models\Model\Attribute as ModelAttribute;

class m151104_112023_create_model_attributes_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(ModelAttribute::tableName(), [
            'model_attribute_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'model_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'attribute_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'UNIQUE INDEX (`model_id`, `attribute_id`)',
            'FOREIGN KEY (`attribute_id`) REFERENCES ' . Attribute::tableName() . ' (`attribute_id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
            'FOREIGN KEY (`model_id`) REFERENCES ' . Model::tableName() . ' (`model_id`) ON DELETE NO ACTION ON UPDATE NO ACTION'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(ModelAttribute::tableName());
    }
}
