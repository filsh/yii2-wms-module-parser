<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\parser\models\Model\Attribute as ModelAttribute;
use wms\parser\models\Model\Attribute\Value as ModelAttributeValue;

class m151104_112035_create_model_attribute_values_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(ModelAttributeValue::tableName(), [
            'model_attribute_value_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'model_attribute_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'model_attribute_value' => Schema::TYPE_STRING . '(255) NOT NULL',
            'FOREIGN KEY (`model_attribute_id`) REFERENCES ' . ModelAttribute::tableName() . ' (`model_attribute_id`) ON DELETE NO ACTION ON UPDATE NO ACTION'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(ModelAttributeValue::tableName());
    }
}
