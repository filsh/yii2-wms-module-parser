<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\parser\models\Attribute;
use wms\parser\models\Attribute\Type;

class m151104_112011_create_attributes_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(Attribute::tableName(), [
            'attribute_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'attribute_type_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'attribute_name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'FOREIGN KEY (`attribute_type_id`) REFERENCES ' . Type::tableName() . ' (`attribute_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(Attribute::tableName());
    }
}
