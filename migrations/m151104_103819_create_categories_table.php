<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\parser\models\Category;

class m151104_103819_create_categories_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(Category::tableName(), [
            'category_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'category_lft' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'category_rgt' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'category_depth' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'category_name' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(Category::tableName());
    }
}
