<?php

use yii\db\Migration;
use yii\db\Schema;
use wms\parser\models\Owner;
use wms\parser\models\Parser;
use wms\parser\models\Parser\Type as ParserType;

class m151104_112121_create_parsers_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable(Parser::tableName(), [
            'parser_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'owner_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'parser_type_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'parser_settings' => Schema::TYPE_TEXT . ' NOT NULL',
            'FOREIGN KEY (`owner_id`) REFERENCES ' . Owner::tableName() . ' (`owner_id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
            'FOREIGN KEY (`parser_type_id`) REFERENCES ' . ParserType::tableName() . ' (`parser_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(Parser::tableName());
    }
}
