<?php

namespace wms\parser\models;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%categories}}".
 *
 * @property integer $category_id
 * @property integer $category_lft
 * @property integer $category_rgt
 * @property integer $category_depth
 * @property string $category_name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%categories}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_lft', 'category_rgt', 'category_depth', 'category_name'], 'required'],
            [['category_lft', 'category_rgt', 'category_depth'], 'integer'],
            [['category_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => Module::t('app', 'Category ID'),
            'category_lft' => Module::t('app', 'Category Lft'),
            'category_rgt' => Module::t('app', 'Category Rgt'),
            'category_depth' => Module::t('app', 'Category Depth'),
            'category_name' => Module::t('app', 'Category Name'),
        ];
    }
}