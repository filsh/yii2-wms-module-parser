<?php

namespace wms\parser\models;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%parsers}}".
 *
 * @property integer $parser_id
 * @property integer $owner_id
 * @property integer $parser_type_id
 * @property string $parser_settings
 *
 * @property ParserAttributes[] $parserAttributes
 * @property Attributes[] $attributes
 * @property Owners $owner
 */
class Parser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%parsers}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'parser_type_id', 'parser_settings'], 'required'],
            [['owner_id', 'parser_type_id'], 'integer'],
            [['parser_settings'], 'string'],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Owners::className(), 'targetAttribute' => ['owner_id' => 'owner_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parser_id' => Module::t('app', 'Parser ID'),
            'owner_id' => Module::t('app', 'Owner ID'),
            'parser_type_id' => Module::t('app', 'Parser Type ID'),
            'parser_settings' => Module::t('app', 'Parser Settings'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParserAttributes()
    {
        return $this->hasMany(ParserAttributes::className(), ['parser_id' => 'parser_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes()
    {
        return $this->hasMany(Attributes::className(), ['attribute_id' => 'attribute_id'])->viaTable('{{%parser_attributes}}', ['parser_id' => 'parser_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Owners::className(), ['owner_id' => 'owner_id']);
    }
}