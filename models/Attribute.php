<?php

namespace wms\parser\models;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%attributes}}".
 *
 * @property integer $attribute_id
 * @property integer $attribute_type_id
 * @property string $attribute_name
 *
 * @property AttributeTypes $attributeType
 * @property ModelAttributes[] $modelAttributes
 * @property Models[] $models
 * @property ParserAttributes[] $parserAttributes
 * @property Parsers[] $parsers
 */
class Attribute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%attributes}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_type_id', 'attribute_name'], 'required'],
            [['attribute_type_id'], 'integer'],
            [['attribute_name'], 'string', 'max' => 255],
            [['attribute_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeTypes::className(), 'targetAttribute' => ['attribute_type_id' => 'attribute_type_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_id' => Module::t('app', 'Attribute ID'),
            'attribute_type_id' => Module::t('app', 'Attribute Type ID'),
            'attribute_name' => Module::t('app', 'Attribute Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeType()
    {
        return $this->hasOne(AttributeTypes::className(), ['attribute_type_id' => 'attribute_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelAttributes()
    {
        return $this->hasMany(ModelAttributes::className(), ['attribute_id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModels()
    {
        return $this->hasMany(Models::className(), ['model_id' => 'model_id'])->viaTable('{{%model_attributes}}', ['attribute_id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParserAttributes()
    {
        return $this->hasMany(ParserAttributes::className(), ['attribute_id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParsers()
    {
        return $this->hasMany(Parsers::className(), ['parser_id' => 'parser_id'])->viaTable('{{%parser_attributes}}', ['attribute_id' => 'attribute_id']);
    }
}