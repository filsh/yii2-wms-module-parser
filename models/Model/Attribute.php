<?php

namespace wms\parser\models\Model;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%model_attributes}}".
 *
 * @property integer $model_attribute_id
 * @property integer $model_id
 * @property integer $attribute_id
 *
 * @property ModelAttributeValues[] $modelAttributeValues
 * @property Attributes $attribute
 * @property Models $model
 */
class Attribute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%model_attributes}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id', 'attribute_id'], 'required'],
            [['model_id', 'attribute_id'], 'integer'],
            [['model_id', 'attribute_id'], 'unique', 'targetAttribute' => ['model_id', 'attribute_id'], 'message' => 'The combination of Model ID and Attribute ID has already been taken.'],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attributes::className(), 'targetAttribute' => ['attribute_id' => 'attribute_id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => Models::className(), 'targetAttribute' => ['model_id' => 'model_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_attribute_id' => Module::t('app', 'Model Attribute ID'),
            'model_id' => Module::t('app', 'Model ID'),
            'attribute_id' => Module::t('app', 'Attribute ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelAttributeValues()
    {
        return $this->hasMany(ModelAttributeValues::className(), ['model_attribute_id' => 'model_attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeModel()
    {
        return $this->hasOne(Attributes::className(), ['attribute_id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Models::className(), ['model_id' => 'model_id']);
    }
}