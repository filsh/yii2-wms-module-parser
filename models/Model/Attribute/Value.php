<?php

namespace wms\parser\models\Model\Attribute;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%model_attribute_values}}".
 *
 * @property integer $model_attribute_value_id
 * @property integer $model_attribute_id
 * @property string $model_attribute_value
 *
 * @property ModelAttributes $modelAttribute
 */
class Value extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%model_attribute_values}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_attribute_id', 'model_attribute_value'], 'required'],
            [['model_attribute_id'], 'integer'],
            [['model_attribute_value'], 'string', 'max' => 255],
            [['model_attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => ModelAttributes::className(), 'targetAttribute' => ['model_attribute_id' => 'model_attribute_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_attribute_value_id' => Module::t('app', 'Model Attribute Value ID'),
            'model_attribute_id' => Module::t('app', 'Model Attribute ID'),
            'model_attribute_value' => Module::t('app', 'Model Attribute Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelAttribute()
    {
        return $this->hasOne(ModelAttributes::className(), ['model_attribute_id' => 'model_attribute_id']);
    }
}