<?php

namespace wms\parser\models\Attribute;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%attribute_types}}".
 *
 * @property integer $attribute_type_id
 * @property string $attribute_type_name
 * @property string $attribute_type_title
 *
 * @property Attributes[] $attributes
 */
class Type extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%attribute_types}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_type_name', 'attribute_type_title'], 'required'],
            [['attribute_type_name', 'attribute_type_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_type_id' => Module::t('app', 'Attribute Type ID'),
            'attribute_type_name' => Module::t('app', 'Attribute Type Name'),
            'attribute_type_title' => Module::t('app', 'Attribute Type Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes()
    {
        return $this->hasMany(Attributes::className(), ['attribute_type_id' => 'attribute_type_id']);
    }
}