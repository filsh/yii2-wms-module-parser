<?php

namespace wms\parser\models\Parser;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%parser_attributes}}".
 *
 * @property integer $parser_attribute_id
 * @property integer $parser_id
 * @property integer $attribute_id
 *
 * @property ParserAttributeRules[] $parserAttributeRules
 * @property Attributes $attribute
 * @property Parsers $parser
 */
class Attribute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%parser_attributes}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parser_id', 'attribute_id'], 'required'],
            [['parser_id', 'attribute_id'], 'integer'],
            [['parser_id', 'attribute_id'], 'unique', 'targetAttribute' => ['parser_id', 'attribute_id'], 'message' => 'The combination of Parser ID and Attribute ID has already been taken.'],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attributes::className(), 'targetAttribute' => ['attribute_id' => 'attribute_id']],
            [['parser_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parsers::className(), 'targetAttribute' => ['parser_id' => 'parser_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parser_attribute_id' => Module::t('app', 'Parser Attribute ID'),
            'parser_id' => Module::t('app', 'Parser ID'),
            'attribute_id' => Module::t('app', 'Attribute ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParserAttributeRules()
    {
        return $this->hasMany(ParserAttributeRules::className(), ['parser_attribute_id' => 'parser_attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeModel()
    {
        return $this->hasOne(Attributes::className(), ['attribute_id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParser()
    {
        return $this->hasOne(Parsers::className(), ['parser_id' => 'parser_id']);
    }
}