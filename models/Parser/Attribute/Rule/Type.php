<?php

namespace wms\parser\models\Parser\Attribute\Rule;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%parser_attribute_rule_types}}".
 *
 * @property integer $parser_attribute_rule_type_id
 * @property string $parser_attribute_rule_type_name
 * @property string $parser_attribute_rule_type_title
 *
 * @property ParserAttributeRules[] $parserAttributeRules
 */
class Type extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%parser_attribute_rule_types}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parser_attribute_rule_type_name', 'parser_attribute_rule_type_title'], 'required'],
            [['parser_attribute_rule_type_name', 'parser_attribute_rule_type_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parser_attribute_rule_type_id' => Module::t('app', 'Parser Attribute Rule Type ID'),
            'parser_attribute_rule_type_name' => Module::t('app', 'Parser Attribute Rule Type Name'),
            'parser_attribute_rule_type_title' => Module::t('app', 'Parser Attribute Rule Type Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParserAttributeRules()
    {
        return $this->hasMany(ParserAttributeRules::className(), ['parser_attribute_rule_type_id' => 'parser_attribute_rule_type_id']);
    }
}