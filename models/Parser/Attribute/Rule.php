<?php

namespace wms\parser\models\Parser\Attribute;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%parser_attribute_rules}}".
 *
 * @property integer $parser_attribute_rule_id
 * @property integer $parser_attribute_id
 * @property integer $parser_attribute_rule_type_id
 * @property string $parser_attribute_rule_settings
 *
 * @property ParserAttributeRuleTypes $parserAttributeRuleType
 * @property ParserAttributes $parserAttribute
 */
class Rule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%parser_attribute_rules}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parser_attribute_id', 'parser_attribute_rule_type_id', 'parser_attribute_rule_settings'], 'required'],
            [['parser_attribute_id', 'parser_attribute_rule_type_id'], 'integer'],
            [['parser_attribute_rule_settings'], 'string'],
            [['parser_attribute_rule_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParserAttributeRuleTypes::className(), 'targetAttribute' => ['parser_attribute_rule_type_id' => 'parser_attribute_rule_type_id']],
            [['parser_attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => ParserAttributes::className(), 'targetAttribute' => ['parser_attribute_id' => 'parser_attribute_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parser_attribute_rule_id' => Module::t('app', 'Parser Attribute Rule ID'),
            'parser_attribute_id' => Module::t('app', 'Parser Attribute ID'),
            'parser_attribute_rule_type_id' => Module::t('app', 'Parser Attribute Rule Type ID'),
            'parser_attribute_rule_settings' => Module::t('app', 'Parser Attribute Rule Settings'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParserAttributeRuleType()
    {
        return $this->hasOne(ParserAttributeRuleTypes::className(), ['parser_attribute_rule_type_id' => 'parser_attribute_rule_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParserAttribute()
    {
        return $this->hasOne(ParserAttributes::className(), ['parser_attribute_id' => 'parser_attribute_id']);
    }
}