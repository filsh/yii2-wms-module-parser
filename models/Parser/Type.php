<?php

namespace wms\parser\models\Parser;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%parser_types}}".
 *
 * @property integer $parser_type_id
 * @property string $parser_type_name
 * @property string $parser_type_title
 */
class Type extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%parser_types}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parser_type_name', 'parser_type_title'], 'required'],
            [['parser_type_name', 'parser_type_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parser_type_id' => Module::t('app', 'Parser Type ID'),
            'parser_type_name' => Module::t('app', 'Parser Type Name'),
            'parser_type_title' => Module::t('app', 'Parser Type Title'),
        ];
    }
}