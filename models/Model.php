<?php

namespace wms\parser\models;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%models}}".
 *
 * @property integer $model_id
 * @property integer $owner_id
 * @property integer $category_id
 * @property string $model_name
 *
 * @property ModelAttributes[] $modelAttributes
 * @property Attributes[] $attributes
 * @property ContentCategories $category
 * @property Owners $owner
 */
class Model extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%models}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'category_id', 'model_name'], 'required'],
            [['owner_id', 'category_id'], 'integer'],
            [['model_name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContentCategories::className(), 'targetAttribute' => ['category_id' => 'category_id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Owners::className(), 'targetAttribute' => ['owner_id' => 'owner_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Module::t('app', 'Model ID'),
            'owner_id' => Module::t('app', 'Owner ID'),
            'category_id' => Module::t('app', 'Category ID'),
            'model_name' => Module::t('app', 'Model Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelAttributes()
    {
        return $this->hasMany(ModelAttributes::className(), ['model_id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes()
    {
        return $this->hasMany(Attributes::className(), ['attribute_id' => 'attribute_id'])->viaTable('{{%model_attributes}}', ['model_id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ContentCategories::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Owners::className(), ['owner_id' => 'owner_id']);
    }
}