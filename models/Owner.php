<?php

namespace wms\parser\models;

use Yii;
use wms\parser\Module;

/**
 * This is the model class for table "{{%owners}}".
 *
 * @property integer $owner_id
 * @property string $owner_name
 *
 * @property Models[] $models
 * @property Parsers[] $parsers
 */
class Owner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Module::tablePrefix('{{%owners}}');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_name'], 'required'],
            [['owner_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'owner_id' => Module::t('app', 'Owner ID'),
            'owner_name' => Module::t('app', 'Owner Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModels()
    {
        return $this->hasMany(Models::className(), ['owner_id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParsers()
    {
        return $this->hasMany(Parsers::className(), ['owner_id' => 'owner_id']);
    }
}