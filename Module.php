<?php

namespace wms\parser;

use Yii;
use yii\i18n\PhpMessageSource;

class Module extends \yii\base\Module
{
    const VERSION = '1.0.0-dev';
    
    public $tablePrefix;
    
    public $controllerNamespace = 'wms\parser\controllers';
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }
    
    public function registerTranslations()
    {
        if(!isset(Yii::$app->i18n->translations['wms/parser/*'])) {
            Yii::$app->i18n->translations['wms/parser/*'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
            ];
        }
    }
    
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('wms/parser/' . $category, $message, $params, $language);
    }
    
    public static function tablePrefix($name)
    {
        if(($module = Module::getInstance()) === null || $module->tablePrefix === null) {
            return $name;
        }
        
        if (strpos($name, '{{') !== false) {
            $name = preg_replace('/\\{\\{(.*?)\\}\\}/', '\1', $name);

            return '{{' . str_replace('%', '%' . $module->tablePrefix, $name) . '}}';
        } else {
            return $name;
        }
    }
}